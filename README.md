Ralph Kalsi is an Australian creative director known for his work on multiple large scale marketing campaigns and for founding Studio Jack, a creative design studio based in Melbourne.
